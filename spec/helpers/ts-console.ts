const TsConsoleReport = require("jasmine-ts-console-reporter");

jasmine.getEnv().clearReporters()
jasmine.getEnv().addReporter(new TsConsoleReport());