
import usuario from "../../src/dominio/usuario";
import injetor = require("../../src/utils/Injector")

describe("criar usuario", () => {
    it("primeiro teste", () => {
        expect(false).toBe(true);
    });
    
    it("segundo teste", () => {
        expect(false).toBe(false);
    });
})

describe("atualizar usuario", () => {
    
})

describe("deletar usuario", () => {
    
})

describe("listar usuario", () => {
    it("", async () => {
        expect( true ).toBe( (await injetor.usuarioReporitorio.listar()).length > 1 );
    })
    
})

/*
async function testarInsercao(){
    let listaOriginal: usuario[] = await injetor.usuarioReporitorio.listar();
    let result: boolean = await injetor.usuarioReporitorio.criar(new usuario(null, "nomeInsercao", "emailInsercao", "senhaInsercao", "facebookInsercao", "saltInsercao"));
    if(result){
        let listaModificada: usuario[] = await injetor.usuarioReporitorio.listar();
        if(listaModificada.length == listaOriginal.length){
            console.error("não foi possivel criar usuario");
        }else{
            let usuarioToDelte: usuario = listaModificada[listaModificada.length-1];
            let resultDelet: boolean = await injetor.usuarioReporitorio.deletar(usuarioToDelte);
            console.log(resultDelet ? "Tudo certo" : "não foi possivel deletar");
        }
    }else{
        console.error("não foi possivel criar usuario");
    }
}
*/