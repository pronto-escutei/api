
import injetor = require("../../src/utils/Injector");

import Security from "../../src/utils/Security";

const security: Security = injetor.security;

describe("teste seguranca", () => {

    it("verificação senha deverá ser verdadeira", async () => {
        const senha = "teste123";
        const salt = await security.gerarSalt(senha);
        const hash = await security.gerarHash(senha, salt);
        
        expect(true).toBe( await security.compararValores(senha, salt, hash) );

    });
    
    it("verificação senha deverá ser falsa", async () => {
        const senha = "teste123";
        const salt = await security.gerarSalt(senha);
        const hash = await security.gerarHash(senha, salt);
        
        expect(false).toBe( await security.compararValores("teste321", salt, hash) );
    });
})
