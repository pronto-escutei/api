interface baseMsg{
    msg: string
}
interface basError{
    error: string
}

class SatatusCodeApi{
    OK = function(): number { return 200; }
    Created = function(): number { return 201; }
    Accepted = function(): number { return 202; }
    NoContent = function(): number { return 204; }
    MovedPermanently = function(): number { return 301; }
    Found = function(): number { return 302; }
    SeeOther = function(): number { return 303; }
    NotModified = function(): number { return 304; }
    TemporaryRedirect = function(): number { return 307; }
    BadRequest = function(): number { return 400; }
    Unauthorized = function(): number { return 401; }
    Forbidden = function(): number { return 403; }
    NotFound = function(): number { return 404; }
    MethodNotAllowed = function(): number { return 405; }
    NotAcceptable = function(): number { return 406; }
    PreconditionFailed = function(): number { return 412; }
    UnsupportedMediaType = function(): number { return 415; }
    InternalServerError = function(): number { return 500; }
    NotImplemented = function(): number { return 501; }
}

export default new SatatusCodeApi();