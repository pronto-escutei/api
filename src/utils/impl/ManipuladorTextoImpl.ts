import ManipuladorTexto from "../ManipuladorTexto";

export default class ManipuladorTextoImpl implements ManipuladorTexto {
    verificarTextosVazios(...valores: string[]): boolean{
        try {
            let resultado = true;
            valores.forEach(valor => { if(valor.length == 0) resultado = false; });
            return resultado;
        } catch (error) { return false; }
    }
}