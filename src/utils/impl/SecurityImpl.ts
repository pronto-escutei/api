import bcrypt = require('bcrypt');
import Security from '../Security';

const saltRounds = 10;

export default class SecurityImpl implements Security{

    async gerarSalt(senha: string): Promise<string> {
        let salt = await bcrypt.genSalt(saltRounds);
        salt = await this.gerarHash(senha, salt);
        return salt;
    }
    async gerarHash(senha: string, salt: string): Promise<string> {
        let hash = await bcrypt.hash(senha, salt);
        return hash
    }
    async compararValores(senha: string, salt: string, hash: string): Promise<boolean> {
        let senhahash = await this.gerarHash(senha, salt);
        return senhahash == hash;
    }
}