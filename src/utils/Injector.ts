import UsuarioRepositorio from "../infra/UsuarioRepositorio";
import UsuarioRepositorioImpl from "../infra/Impl/UsuarioRepositorioImpl";
import Security from "./Security";
import SecurityImpl from "./impl/SecurityImpl";
import AutentificacaoService from "../services/AutentificacaoService";
import AutentificacaoServiceImpl from "../services/impl/AutentificacaoServiceImpl";
import UsuarioServiceImpl from "../services/impl/UsuarioServiceImpl";
import UsuarioService from "../services/UsuarioService";
import ManipuladorTexto from "../utils/ManipuladorTexto";
import ManipuladorTextoImpl from "../utils/impl/ManipuladorTextoImpl";
import ComentarioRepositorio from "../infra/ComentarioRepositorio";
import ComentarioRepositorioImpl from "../infra/Impl/ComentarioRepositorioImpl";
import PostRepositorio from "../infra/PostRepositorio";
import PostRepositorioImpl from "../infra/Impl/PostRepositorioImpl";
import ProdutoRepositorio from "../infra/produtoRepositorio";
import ProdutoRepositorioImpl from "../infra/Impl/ProdutoRepositorioImpl";
import LojaRepositorio from "../infra/LojaRepositorio";
import LojaRepositorioImpl from "../infra/Impl/LojaRepositorioImpl";

export let usuarioReporitorio: UsuarioRepositorio = new UsuarioRepositorioImpl();
export let produtoReporitorio: ProdutoRepositorio = new ProdutoRepositorioImpl();
export let lojaReporitorio: LojaRepositorio = new LojaRepositorioImpl();
export let postRepositorio: PostRepositorio = new PostRepositorioImpl();
export let comentarioReporitorio: ComentarioRepositorio = new ComentarioRepositorioImpl();
export let security: Security = new SecurityImpl();
export let manipuladorTexto: ManipuladorTexto = new ManipuladorTextoImpl();

export let UsuarioService: UsuarioService = new UsuarioServiceImpl(usuarioReporitorio, security);
export let autentificacaoService: AutentificacaoService = new AutentificacaoServiceImpl(security, usuarioReporitorio, manipuladorTexto);