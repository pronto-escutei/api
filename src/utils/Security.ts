export default interface Security{
    gerarSalt(senha: string): Promise<string> 
    gerarHash(senha: string, salt: string): Promise<string> 
    compararValores(senha: string, salt: string, hash: string): Promise<boolean> 
}