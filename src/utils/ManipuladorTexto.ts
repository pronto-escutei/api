export default interface ManipuladorTexto{
    verificarTextosVazios(...valores: string[]): boolean
}