import usuario from "../dominio/usuario";

export default interface UsuarioRepositorio extends BaseDao<usuario>{

    criar(objeto: usuario): Promise<boolean>;
    listar(): Promise<usuario[]>;
    atualizar(objeto: usuario): Promise<boolean>;
    deletar(objeto: usuario): Promise<boolean>;

    buscarPorEmail(email: string): Promise<usuario>
    buscarPorId(id: number): Promise<usuario>

}
