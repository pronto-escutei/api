interface BaseDao<T>{

    criar(objeto: T): Promise<boolean>
    listar(): Promise<T[]>
    atualizar(objeto: T): Promise<boolean>
    deletar(objeto: T): Promise<boolean>

}