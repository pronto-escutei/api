import loja from "../dominio/loja";

export default interface LojaRepositorio extends BaseDao<loja>{

    criar(objeto: loja): Promise<boolean>;
    listar(): Promise<loja[]>;
    atualizar(objeto: loja): Promise<boolean>;
    deletar(objeto: loja): Promise<boolean>;
}
