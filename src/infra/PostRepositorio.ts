import post from "../dominio/post";

export default interface PostRepositorio extends BaseDao<post>{

    criar(objeto: post): Promise<boolean>;
    listar(): Promise<post[]>;
    atualizar(objeto: post): Promise<boolean>;
    deletar(objeto: post): Promise<boolean>;
}
