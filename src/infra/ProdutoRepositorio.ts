import produto from "../dominio/produto";

export default interface ProdutoRepositorio extends BaseDao<produto>{

    criar(objeto: produto): Promise<boolean>;
    listar(): Promise<produto[]>;
    atualizar(objeto: produto): Promise<boolean>;
    deletar(objeto: produto): Promise<boolean>;
}
