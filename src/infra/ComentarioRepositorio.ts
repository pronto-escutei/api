import comentario from "../dominio/comentario";

export default interface ComentarioRepositorio extends BaseDao<comentario>{

    criar(objeto: comentario): Promise<boolean>;
    listar(): Promise<comentario[]>;
    listarPorPost(post:string): Promise<comentario[]>;
    atualizar(objeto: comentario): Promise<boolean>;
    deletar(objeto: comentario): Promise<boolean>;
}
