import dbConfig from "../../config/DbConfig"
import { Connection } from "oracledb";
const oracledb = require('oracledb');

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

export default class ConexaoFabrica{
    
    async abrirConexao(): Promise<Connection>{
        let prop = {
            user          : dbConfig.user,
            password      : dbConfig.bdSenha,
            connectString : dbConfig.connectString
        };
        try{
            return oracledb.getConnection( prop );
        }catch(err){
            console.error(err);
        }
    }

    async fecharConexao(connection: Connection){
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }

    async executarQuery(query: string, parametros: string[]): Promise<any> {
        let conexao: Connection;
        try{
            conexao = await this.abrirConexao();
            if(parametros && parametros.length > 0){
                let result = await conexao.execute( query, parametros, { autoCommit: true}, );
                return result;
            }else{
                return await conexao.execute( query );
            }
        }catch(err){
            console.error(err);
        }finally{
            this.fecharConexao(conexao);
        } 
    }
}