export default class GeradorScriptSqlBasico<T>{


    private getNomeTabela(objeto: T): string{
        return `uclpi.${objeto.constructor.name}`;
    } 

    private getNomeTabelaByStr(objeto: string): string{
        return `uclpi.${objeto}`;
    }

    listar(nomeTabela: string): string{
        return `select * from ${this.getNomeTabelaByStr(nomeTabela)}`;
    }

    criar(objeto: T, parametros: string[], valores: string[]): string{
        if(parametros.findIndex( x => { x.toLowerCase().includes("id") }) == -1){
            parametros.push("id");
            valores.push(`${this.getNomeTabela(objeto)}_sequence.nextval`);
        }
        return `insert into ${this.getNomeTabela(objeto)}(${parametros.join(',')})values(${valores.map( (x) => { return x.startsWith(":") ? `${x}` : x } ).join(',')})`;
    }

    atualizar(objeto: T, parametros: string[], valores: string[]): string{
        let query: string = `update ${this.getNomeTabela(objeto)} set `;
        for (let index = 0; index < parametros.length; index++) {
              query += `${parametros[index]} = ${valores[index]}`; 
              if(index != parametros.length-1) query += ","
              query += " ";
            }
        query += this.getidentificador();
        return query;
    }

    deletar(nomeTabela: string): string{
        return `delete from ${this.getNomeTabelaByStr(nomeTabela)} where id = :id`;
    }

    getidentificador(): string{
        return `where id = :id`;
    }

}
