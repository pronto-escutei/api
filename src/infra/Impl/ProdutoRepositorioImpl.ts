import produto from "../../dominio/produto";
import ConexaoFabrica from "./ConexaoFabrica";
import GeradorScriptSqlBasico from "./GeradorScriptSqlBasico";
import injecao = require("../../utils/Injector");
import ProdutoRepositorio from "../produtoRepositorio";

export default class ProdutoRepositorioImpl implements ProdutoRepositorio {

    _fabrica: ConexaoFabrica
    _geradorScript: GeradorScriptSqlBasico<produto>

    constructor(){
        this._fabrica = new ConexaoFabrica();
        this._geradorScript = new GeradorScriptSqlBasico<produto>();
    }

    async criar(objeto: produto): Promise<boolean>{
        let query: string = this._geradorScript.criar(objeto, ["LOJA_ID", "LINK", "NOME", "IMAGEM"], [":LOJA_ID", ":LINK", ":NOME", ":IMAGEM"]);
        let result = await this._fabrica.executarQuery( query, [objeto.LOJA.ID, objeto.LINK, objeto.NOME, objeto.IMAGEM] )
        return result.rowsAffected > 0;
    }
    async listar(): Promise<produto[]>{
        let query: string = this._geradorScript.listar(produto.name);
        let result = await this._fabrica.executarQuery( query, [] );
        let produtos: produto[] = [];
        for (let i = 0; i < result.rows.length; i++) {
            let produtoDomain: produto = new produto();
            produtoDomain.LOJA = (await injecao.lojaReporitorio.listar()).find( (x) => x.ID == result.rows[i].LOJA_ID );
            produtoDomain.ID = result.rows[i].ID;
            produtoDomain.IMAGEM = result.rows[i].IMAGEM;
            produtoDomain.LINK = result.rows[i].LINK;
            produtoDomain.NOME = result.rows[i].NOME;
            produtos.push(produtoDomain);
        }
        return produtos;
    }
    async atualizar(objeto: produto): Promise<boolean>{
        let query: string = this._geradorScript.atualizar(objeto, ["LOJA_ID", "LINK", "NOME", "IMAGEM"], [":LOJA_ID", ":LINK", ":NOME", ":IMAGEM"]);
        let result = await this._fabrica.executarQuery( query, [objeto.LOJA.ID, objeto.LINK, objeto.NOME, objeto.IMAGEM] )
        return result.rowsAffected > 0;
    }
    async deletar(objeto: produto): Promise<boolean>{
        let query: string = this._geradorScript.deletar( "produto");
        let result = await this._fabrica.executarQuery( query, [objeto.ID.toString()] );
        return result.rowsAffected > 0;
    }
}