import loja from "../../dominio/loja";
import ConexaoFabrica from "./ConexaoFabrica";
import GeradorScriptSqlBasico from "./GeradorScriptSqlBasico";
import injecao = require("../../utils/Injector");
import LojaRepositorio from "../LojaRepositorio";

export default class LojaRepositorioImpl implements LojaRepositorio {

    _fabrica: ConexaoFabrica
    _geradorScript: GeradorScriptSqlBasico<loja>

    constructor(){
        this._fabrica = new ConexaoFabrica();
        this._geradorScript = new GeradorScriptSqlBasico<loja>();
    }

    async criar(objeto: loja): Promise<boolean>{
        let query: string = this._geradorScript.criar(objeto, ["NOME", "LINK", "IMAGEM"], [":NOME", ":LINK", ":IMAGEM"]);
        let result = await this._fabrica.executarQuery( query, [objeto.NOME, objeto.LINK, objeto.IMAGEM] )
        return result.rowsAffected > 0;
    }
    async listar(): Promise<loja[]>{
        let query: string = this._geradorScript.listar(loja.name);
        let result = await this._fabrica.executarQuery( query, [] );
        let lojas: loja[] = [];
        for (let i = 0; i < result.rows.length; i++) {
            let lojaDomain: loja = new loja();
            lojaDomain.ID = result.rows[i].ID;
            lojaDomain.IMAGEM = result.rows[i].IMAGEM;
            lojaDomain.LINK = result.rows[i].LINK;
            lojaDomain.NOME = result.rows[i].NOME;
            lojas.push(lojaDomain);
        }
        return lojas;
    }
    async atualizar(objeto: loja): Promise<boolean>{
        let query: string = this._geradorScript.atualizar(objeto, [ "LINK", "NOME", "IMAGEM"], [":LINK", ":NOME", ":IMAGEM"]);
        let result = await this._fabrica.executarQuery( query, [objeto.LINK, objeto.NOME, objeto.IMAGEM] )
        return result.rowsAffected > 0;
    }
    async deletar(objeto: loja): Promise<boolean>{
        let query: string = this._geradorScript.deletar( "loja");
        let result = await this._fabrica.executarQuery( query, [objeto.ID.toString()] );
        return result.rowsAffected > 0;
    }
}