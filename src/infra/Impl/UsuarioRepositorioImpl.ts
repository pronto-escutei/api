import usuario from "../../dominio/usuario";
import ConexaoFabrica from "./ConexaoFabrica";
import GeradorScriptSqlBasico from "./GeradorScriptSqlBasico";
import UsuarioRepositorio from "../UsuarioRepositorio";

export default class UsuarioRepositorioImpl implements UsuarioRepositorio {

    _fabrica: ConexaoFabrica
    _geradorScript: GeradorScriptSqlBasico<usuario>

    constructor(){
        this._fabrica = new ConexaoFabrica();
        this._geradorScript = new GeradorScriptSqlBasico<usuario>();
    }

    async criar(objeto: usuario): Promise<boolean>{
        let query: string = this._geradorScript.criar(objeto, ["nome", "email", "senha", "id_facebook", "salt"], [":nome", ":email", ":senha", ":id_facebook", ":salt"]);
        let result = await this._fabrica.executarQuery( query, [ objeto.NOME, objeto.EMAIL, objeto.SENHA, objeto.ID_FACEBOOK, objeto.SALT ] )
        return result.rowsAffected > 0;
    }
    async listar(): Promise<usuario[]>{
        let query: string = this._geradorScript.listar(usuario.name);
        let result = await this._fabrica.executarQuery( query, [] );
        return result.rows.map((user: usuario ) => { return user } );
    }
    async atualizar(objeto: usuario): Promise<boolean>{
        let listaParam: string[] = ["nome", "email", "foto"];
        let listavalues: string[] = [objeto.NOME, objeto.EMAIL, objeto.FOTO];
        if(objeto.SENHA && objeto.SALT){
            listaParam.concat("senha", "salt");
            listavalues.concat(objeto.SENHA, objeto.SALT);
        }
        listavalues.push(objeto.ID.toString());
        let query: string = this._geradorScript.atualizar(objeto, listaParam, listaParam.map( x => ":"+x ));
        let result = await this._fabrica.executarQuery( query, listavalues )
        return result.rowsAffected > 0;
    }

    async deletar(objeto: usuario): Promise<boolean>{
        let query: string = this._geradorScript.deletar( "usuario");
        let result = await this._fabrica.executarQuery( query, [objeto.ID.toString()] );
        return result.rowsAffected > 0;
    }
    
    async buscarPorId(id: number): Promise<usuario>{
        let query = "select * from uclpi.usuario where id = :id";
        let result = await this._fabrica.executarQuery( query, [id.toString()] );
        return result.rows.length > 0 ? result.rows[0] : null;
    }

    async buscarPorEmail(email: string): Promise<usuario>{
        let query = "select * from uclpi.usuario where email = :email";
        let result = await this._fabrica.executarQuery( query, [email] );
        return result.rows.length > 0 ? result.rows[0] : null;
    }
}