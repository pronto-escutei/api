import comentario from "../../dominio/comentario";
import ConexaoFabrica from "./ConexaoFabrica";
import GeradorScriptSqlBasico from "./GeradorScriptSqlBasico";
import ComentarioRepositorio from "../ComentarioRepositorio";
import injecao = require("../../utils/Injector");
import usuario from "../../dominio/usuario";

export default class comentarioRepositorioImpl implements ComentarioRepositorio {

    _fabrica: ConexaoFabrica
    _geradorScript: GeradorScriptSqlBasico<comentario>

    constructor(){
        this._fabrica = new ConexaoFabrica();
        this._geradorScript = new GeradorScriptSqlBasico<comentario>();
    }

    async criar(objeto: comentario): Promise<boolean>{
        let query: string = this._geradorScript.criar(objeto, ["CONTEUDO", "POST_ID", "USUARIO_ID"], [":CONTEUDO", ":POST_ID", ":USUARIO_ID"]);
        let result = await this._fabrica.executarQuery( query, [ objeto.CONTEUDO, objeto.POST.ID, objeto.USUARIO.ID.toString() ] )
        return result.rowsAffected > 0;
    }
    async listar(): Promise<comentario[]>{
        
        let query: string = ` select u.*, c.* from UCLPI.comentario c  inner join UCLPI.usuario u on u.id = c.usuario_id`;

        let result = await this._fabrica.executarQuery( query, [] );
        
        let comentarios: comentario[] = [];
        for (let i = 0; i < result.rows.length; i++) {
            let comment: comentario = new comentario();
            comment.POST = (await injecao.postRepositorio.listar()).find( (x) => x.ID == result.rows[i].POST_ID );
            let user: usuario = new usuario( injecao.manipuladorTexto );
            
            user.EMAIL = result.rows[i].EMAIL;
            user.FOTO = result.rows[i].FOTO;
            user.ID = result.rows[i].ID;
            user.NOME = result.rows[i].NOME;

            comment.USUARIO = user;
            comment.CONTEUDO = result.rows[i].CONTEUDO;
            comment.ID = result.rows[i].ID;
            comentarios.push(comment);
        }
        return comentarios;
    }
    async atualizar(objeto: comentario): Promise<boolean>{
        let query: string = this._geradorScript.atualizar(objeto, ["CONTEUDO", "POST_ID", "USUARIO_ID"], [":CONTEUDO", ":POST_ID", ":USUARIO_ID"]);
        let result = await this._fabrica.executarQuery( query, [ objeto.CONTEUDO, objeto.POST.ID, objeto.USUARIO.ID.toString() ] )
        return result.rowsAffected > 0;
    }
    async deletar(objeto: comentario): Promise<boolean>{
        let query: string = this._geradorScript.deletar( "comentario");
        let result = await this._fabrica.executarQuery( query, [objeto.ID.toString()] );
        return result.rowsAffected > 0;
    }
    async listarPorPost(post: string): Promise<comentario[]>{
        let comentarios: comentario[] = [];
        let query: string = ` select u.*, c.* from UCLPI.comentario c  inner join UCLPI.usuario u on u.id = c.usuario_id where c.post_id = '${post}' `;
        try {
            let result = await this._fabrica.executarQuery( query, [] );
        for (let i = 0; i < result.rows.length; i++) {
            let comment: comentario = new comentario();
            comment.POST = (await injecao.postRepositorio.listar()).find( (x) => x.ID == result.rows[i].POST_ID );
            let user: usuario = new usuario( injecao.manipuladorTexto );
            
            user.EMAIL = result.rows[i].EMAIL;
            user.FOTO = result.rows[i].FOTO;
            user.ID = result.rows[i].ID;
            user.NOME = result.rows[i].NOME;

            comment.USUARIO = user;
            comment.CONTEUDO = result.rows[i].CONTEUDO;
            comment.ID = result.rows[i].ID;
            comentarios.push(comment);
        }
        } catch (error) {
            console.log(error);
        }
        return comentarios;
    }
}