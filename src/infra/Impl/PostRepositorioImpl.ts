import post from "../../dominio/post";
import ConexaoFabrica from "./ConexaoFabrica";
import GeradorScriptSqlBasico from "./GeradorScriptSqlBasico";
import PostRepositorio from "../PostRepositorio";
import injecao = require("../../utils/Injector");
import produto from "../../dominio/produto";
import usuario from "../../dominio/usuario";
import loja from "../../dominio/loja";

export default class PostRepositorioImpl implements PostRepositorio {

    _fabrica: ConexaoFabrica
    _geradorScript: GeradorScriptSqlBasico<post>

    constructor(){
        this._fabrica = new ConexaoFabrica();
        this._geradorScript = new GeradorScriptSqlBasico<post>();
    }

    async criar(objeto: post): Promise<boolean>{
        let query: string = this._geradorScript.criar(objeto, ["CONTEUDO", "ID_USUARIO", "ID_PRODUTO"], [":CONTEUDO", ":ID_USUARIO", ":ID_PRODUTO"]);
        let result = await this._fabrica.executarQuery( query, [ objeto.CONTEUDO, objeto.USUARIO.ID.toString(), objeto.PRODUTO.ID.toString() ] )
        return result.rowsAffected > 0;
    }
    async listar(): Promise<post[]>{
        let query: string = `
        select po.*, pr.*, l.*, u.*,
            u.id userId, po.id prId, po.id poID, l.id lId, u.nome uNome, l.nome lNome, pr.nome prNome, l.link lLink, pr.link prLinik, pr.imagem prImagem
        from UCLPI.post po
        inner join UCLPI.usuario u on u.id = po.id_usuario
        inner join UCLPI.produto pr on pr.id = po.id_produto
        inner join UCLPI.loja l on l.id = pr.loja_id
        `;
        let result = await this._fabrica.executarQuery( query, [] );
        let resultado: post[] = [];
        for (let i = 0; i < result.rows.length; i++) {
            let postDomain: post = new post();
            postDomain.CONTEUDO = result.rows[i].CONTEUDO;
            postDomain.ID = result.rows[i].POID;
            postDomain.USUARIO = new usuario( injecao.manipuladorTexto );
            postDomain.USUARIO.ID = result.rows[i].USERID;
            postDomain.USUARIO.EMAIL = result.rows[i].EMAIL;
            postDomain.USUARIO.FOTO = result.rows[i].FOTO;
            postDomain.USUARIO.NOME = result.rows[i].UNOME;

            postDomain.PRODUTO = new produto();
            postDomain.PRODUTO.ID = result.rows[i].PRID;
            postDomain.PRODUTO.IMAGEM = result.rows[i].PRIMAGEM;
            postDomain.PRODUTO.LINK = result.rows[i].PRLINK;
            postDomain.PRODUTO.NOME = result.rows[i].PRNOME;
            postDomain.PRODUTO.LOJA = new loja();

            postDomain.PRODUTO.LOJA.ID = result.rows[i].LID;
            postDomain.PRODUTO.LOJA.IMAGEM = result.rows[i].LIMAGEM;
            postDomain.PRODUTO.LOJA.LINK = result.rows[i].LLINK;
            postDomain.PRODUTO.LOJA.NOME = result.rows[i].LNOME;

            resultado.push(postDomain);
        }
        return resultado;
    }
    async atualizar(objeto: post): Promise<boolean>{
        let query: string = this._geradorScript.atualizar(objeto, ["CONTEUDO", "ID_USUARIO", "ID_PRODUTO"], [":CONTEUDO", ":ID_USUARIO", ":ID_PRODUTO"]);
        let result = await this._fabrica.executarQuery( query, [ objeto.CONTEUDO, objeto.USUARIO.ID.toString(), objeto.PRODUTO.ID.toString() ] )
        return result.rowsAffected > 0;
    }
    async deletar(objeto: post): Promise<boolean>{
        let query: string = this._geradorScript.deletar( "post");
        let result = await this._fabrica.executarQuery( query, [objeto.ID.toString()] );
        return result.rowsAffected > 0;
    }
}