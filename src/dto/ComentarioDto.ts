import usuarioDto from "./usuarioDto";
import IdentificadorDto from "./IdentificadorDto";

export class UsuarioComentarioDto{
    nome: string;
    id: number;
    foto: string;
}

export default class ComentarioDto{
    usuario: UsuarioComentarioDto;
    avaliacaoPositiva: number;
    avaliacaoNegativa: number;
    avaliacaoAtual: number;
    Conteudo: string;
}