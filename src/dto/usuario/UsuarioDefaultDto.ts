export default class UsuarioDefaultDto{
    ID: number;
    NOME: string;
    EMAIL: string;
    FOTO: string;
    SENHA?: string;
}
