
export default class LojaDto{
    id: number;
    nome: string;
    link: string;
    imagem: string;
}