import injecao = require("../utils/Injector");

export default class LoginDto{
    login: string;
    senha: string;
    constructor(){}

    static validarCamposObrigatorios(usuario: LoginDto): boolean{
        return injecao.manipuladorTexto.verificarTextosVazios(usuario.login, usuario.senha);
    }
}