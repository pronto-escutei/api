import IdentificadorDto from "./IdentificadorDto";

export default class ProdutoDto{
    LOJA_ID: number;
    ID: number;
    LINK: string;
    NOME: string;
    IMAGEM: string;
    NOMELOJA: string;
}
