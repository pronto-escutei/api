
export default class PostDto{
    id: number;
    conteudo: string;
    fotoProduto: string;
    avaliacaoPositiva: number;
    avaliacaoNegativa: number;
    avaliacaoAtual: number;
    fotoUsuario: string;
    idUsuario: number;
}