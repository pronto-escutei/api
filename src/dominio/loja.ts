import ManipuladorTexto from "../utils/ManipuladorTexto";

export default class loja implements BaseModel{
    ID: string;
    NOME: string;
    LINK: string;
    IMAGEM: string;

    fromAnother(loja: loja){
        this.ID = loja.ID;
        this.NOME = loja.NOME;
        this.LINK = loja.LINK;
        this.IMAGEM = loja.IMAGEM;
    }
}