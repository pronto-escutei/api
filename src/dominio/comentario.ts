import post from "./post"
import usuario from "./usuario"
import ManipuladorTexto from "../utils/ManipuladorTexto";

export default class comentario implements BaseModel{

    ID: number;
    USUARIO: usuario;
    POST: post;
    CONTEUDO: string;

    private _manipuladorTexto: ManipuladorTexto;
}