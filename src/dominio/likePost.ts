import post from "./post"
import usuario from "./usuario"
import ManipuladorTexto from "../utils/ManipuladorTexto";

export default class likePost implements BaseModel{
    ID: number;
    USUARIO: usuario;
    POST: post;
    POSITIVO: boolean;
    
    constructor(){}
    
    public validarCamposObrigatorios(): boolean{ 
        return this.POST != null && this.USUARIO != null;
     }
}