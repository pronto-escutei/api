import ManipuladorTexto from "../utils/ManipuladorTexto";

export default class usuario implements BaseModel{
    
    ID: number;
    NOME: string;
    EMAIL: string;
    ID_FACEBOOK: string;
    SENHA: string;
    SALT: string;
    FOTO: string;

    private _manipuladorTexto: ManipuladorTexto;

    constructor(manipuladorTexto: ManipuladorTexto){
        this._manipuladorTexto = manipuladorTexto;
    }

    fromAnother(usuario: usuario){
        this.ID = usuario.ID;
        this.NOME = usuario.NOME;
        this.EMAIL = usuario.EMAIL;
        this.ID_FACEBOOK = usuario.ID_FACEBOOK;
        this.SENHA = usuario.SENHA;
        this.SALT = usuario.SALT;
    }

    public validarCamposObrigatorios(): boolean{
        return this._manipuladorTexto.verificarTextosVazios(this.NOME, this.EMAIL, this.SENHA, this.SENHA, this.SALT);
    }
}