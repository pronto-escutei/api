import post from "./post"
import ManipuladorTexto from "../utils/ManipuladorTexto";

export default class adicional{
    
    ID: number;
    POST: post;
    CONTEUDO: string;

    private _manipuladorTexto: ManipuladorTexto;

    constructor(manipuladorTexto: ManipuladorTexto){
        this._manipuladorTexto = manipuladorTexto;
    }

    public validarCamposObrigatorios(): boolean{ return this._manipuladorTexto.verificarTextosVazios(this.CONTEUDO); }

}