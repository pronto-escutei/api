import usuario from "./usuario"
import ManipuladorTexto from "../utils/ManipuladorTexto";

export default class amizade implements BaseModel{
    ID: number;
    SOLICITANTE: usuario;
    SOLICITADO: usuario;
    aceito: boolean;

    constructor(){ }

    public validarCamposObrigatorios(): boolean{ return this.SOLICITANTE != null && this.SOLICITADO != null }
}