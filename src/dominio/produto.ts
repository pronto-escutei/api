import loja from "./loja"
import ManipuladorTexto from "../utils/ManipuladorTexto";

export default class produto implements BaseModel{

    ID: number;
    LOJA: loja;
    LINK: string;
    NOME: string;
    IMAGEM: string;
}