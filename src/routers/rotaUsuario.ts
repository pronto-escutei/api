import express = require("express");
const router = express.Router();
import { Request, Response } from 'express';
import authMiddleware from "../middleware/auth"
import { UserAuthInfoRequest } from "../utils/UserAuthInfoRequest";

router.use(authMiddleware);
router.get("/", (req: UserAuthInfoRequest, res: Response) => {  console.log(`userId: ${req.userId}`); res.send("teste inicial usuario") })

module.exports = router;