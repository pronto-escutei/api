import express = require("express");

// rotas
const autentificacao = require('./rotaAutentificacao');
const usuario = require('./rotaUsuario');
const post = require('./rotaPost');
const comentario = require('./rotaComentario');
const loja = require('./rotaLoja');
const produto = require('./rotaProduto');

const statusController = require("../controllers/StatusController");
const router = express.Router();

router.use("/", statusController)

router.use("/autentificacao", autentificacao);
router.use("/usuario", usuario);
router.use("/post", post);
router.use("/comentario", comentario);
router.use("/loja", loja);
router.use("/produto", produto);

module.exports = router;