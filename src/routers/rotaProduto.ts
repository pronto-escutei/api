import express = require("express");
const router = express.Router();
import { Response } from 'express';
import authMiddleware from "../middleware/auth"
import { UserAuthInfoRequest } from "../utils/UserAuthInfoRequest";

router.use(authMiddleware);
router.get("/", (req: UserAuthInfoRequest, res: Response) => {  res.send("teste inicial produto") })


module.exports = router;