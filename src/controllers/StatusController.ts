import app from "../app";
import { Controller, Route, Get, Post, Security } from 'tsoa';
import SatatusCode from "../utils/impl/StatusCode";

@Route("/status")
export class StatusController extends Controller{
    
    @Security('jwt')
    @Get("/")
    public async status(): Promise<string>{
        try {
            const msg = `App is running on ${app.get("port")}, in ${app.get("env")} mode `;
            this.setStatus(SatatusCode.OK() );
            return msg;
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return error.message;
        }
    }
}
