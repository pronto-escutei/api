import { Controller, Route, Get, Post, Security, Put, Delete, Body, Request } from 'tsoa';
import SatatusCode from "../utils/impl/StatusCode";
import injecao = require("../utils/Injector");
import mensagens = require("./mensagens");
import ComentarioDto, { UsuarioComentarioDto } from '../dto/ComentarioDto';
import ComentarioCriacaoDto from '../dto/comentario/ComentarioCriacaoDto';
import ComentarioAlteracaoDto from '../dto/comentario/ComentarioAlteracaoDto';
import AvaliacaoDto from '../dto/AvaliacaoDto';
import comentario from '../dominio/comentario';
import post from '../dominio/post';

@Route("/comentario")
export class ComentarioController extends Controller{
    
    public converterComentarioDro(comentarioDoamin: comentario): ComentarioDto{
        let comentario: ComentarioDto = new ComentarioDto();
        comentario.Conteudo = comentarioDoamin.CONTEUDO;
        comentario.avaliacaoNegativa = 10;
        comentario.avaliacaoPositiva = 10;
        comentario.avaliacaoAtual = 0;
        comentario.usuario = new UsuarioComentarioDto();
        comentario.usuario.nome = comentarioDoamin.USUARIO.NOME;
        comentario.usuario.foto = comentarioDoamin.USUARIO.FOTO;
        comentario.usuario.id = comentarioDoamin.ID;
        return comentario;
    }

    @Security('jwt')
    @Get("/{post}")
    public async listar( post: string ): Promise<ComentarioDto[]>{
        try {
            this.setStatus(SatatusCode.OK());
            let comentarios: comentario[] = await injecao.comentarioReporitorio.listarPorPost(post);
            comentarios = comentarios.filter( x => x.POST.ID = post );

            return comentarios.map( x => this.converterComentarioDro(x) );
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return [];
        }
    }
    
    @Security('jwt')
    @Post("/")
    public async criar( @Request() request: Express.Request, @Body() comentarioCriacaoDto: ComentarioCriacaoDto ): Promise<string>{
        try {
            const userId: string = ( (<any>request).userId );
            let comentarioDomain : comentario = new comentario();
            comentarioDomain.CONTEUDO = comentarioCriacaoDto.Conteudo;
            let posts: post[] = (await injecao.postRepositorio.listar());
            comentarioDomain.POST = posts.find( (x) => x.ID == comentarioCriacaoDto.Postid );
            comentarioDomain.USUARIO = await injecao.UsuarioService.buscarPorId( parseInt(userId) );
            injecao.comentarioReporitorio.criar(comentarioDomain);
            this.setStatus(SatatusCode.OK());
            return mensagens.operacaoSucesso;
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return error.message;
        }
    }
    
    @Security('jwt')
    @Put("/")
    public async alterar( @Body() comentarioAlteracaoDto: ComentarioAlteracaoDto ): Promise<string>{
        try {
            this.setStatus(SatatusCode.OK());
            return mensagens.operacaoSucesso;
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return error.message;
        }
    }

    @Security('jwt')
    @Delete("/{comentarioId}")
    public async deletar( comentarioId: string ): Promise<string>{
        try {
            this.setStatus(SatatusCode.OK());
            return mensagens.operacaoSucesso;
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return error.message;
        }
    }
    
    @Security('jwt')
    @Post("/avaliar")
    public async avaliar( @Body() avaliacao: AvaliacaoDto ): Promise<string>{
        this.setStatus(SatatusCode.OK());
        return mensagens.operacaoSucesso;
    }
    
}
