import { Controller, Route, Get, Post, Security, Body, Put, Request } from 'tsoa';
import SatatusCode from "../utils/impl/StatusCode";
import UsuarioDefaultDto from '../dto/usuario/UsuarioDefaultDto';
import IdentificadorDto from '../dto/IdentificadorDto';
import injecao = require("../utils/Injector");

import mensagens = require("./mensagens");
import usuario from '../dominio/usuario';

@Route("/usuario")
export class UsuarioController extends Controller{
    
    @Security('jwt')
    @Get("/")
    public async status(
        @Request() request: Express.Request
    ): Promise<UsuarioDefaultDto>{
        try {
            const userId: string = ( (<any>request).userId );
            const user: UsuarioDefaultDto = new UsuarioDefaultDto();
            const userDomain: usuario = await injecao.usuarioReporitorio.buscarPorId( Number.parseInt(userId) );
            if(userDomain){
                this.setStatus(SatatusCode.OK());
                user.EMAIL = userDomain.EMAIL;
                user.FOTO = userDomain.FOTO;
                user.ID = userDomain.ID;
                user.NOME = userDomain.NOME;
            }else{
                this.setStatus(SatatusCode.NotFound());
            }
            return user;
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return new UsuarioDefaultDto();
        }
    }

    
    @Security('jwt')
    @Put("/alterar")
    public async alterar(  @Body() usuarioDtoResponse: UsuarioDefaultDto ): Promise<string>{
        this.setStatus(SatatusCode.OK());
        try {
            let usuarioDomain: usuario = new usuario(injecao.manipuladorTexto);
            usuarioDomain.EMAIL = usuarioDtoResponse.EMAIL;
            usuarioDomain.ID = usuarioDtoResponse.ID;
            usuarioDomain.FOTO = usuarioDtoResponse.FOTO;
            usuarioDomain.NOME = usuarioDtoResponse.NOME;
            usuarioDomain.SENHA = usuarioDtoResponse.SENHA;
            if( injecao.UsuarioService.alterar(usuarioDomain)) this.setStatus(SatatusCode.OK());
            else this.setStatus(SatatusCode.InternalServerError());
            return mensagens.operacaoSucesso;
        } catch (error) {
            console.log(error);
            this.setStatus(SatatusCode.InternalServerError());
            return mensagens.operacaoFalha;
        }
    }
    
    @Security('jwt')
    @Post("/pedirAmizade")
    public async pedirAmizade(@Body() identificador: IdentificadorDto): Promise<string>{
        this.setStatus(SatatusCode.OK());
        return mensagens.operacaoSucesso;

    }
    
    @Security('jwt')
    @Post("/aceitarAmizade")
    public async aceitarAmizade(@Body() identificador: IdentificadorDto): Promise<string>{
        this.setStatus(SatatusCode.OK());
        return mensagens.operacaoSucesso;
    }

}
