import { Controller, Route, Get, Post, Security, Body, Put, Delete, Request } from 'tsoa';
import SatatusCode from "../utils/impl/StatusCode";
import PostDto from '../dto/PostDto';
import PostCriacaoDto from '../dto/post/PostCriacaoDto';

import mensagens = require("./mensagens");
import injecao = require("../utils/Injector");
import AvaliacaoDto from '../dto/AvaliacaoDto';
import post from '../dominio/post';
import ProdutoDto from '../dto/ProdutoDto';
import produto from '../dominio/produto';
import usuario from '../dominio/usuario';

@Route("/post")
export class PostController extends Controller{

    public criarPostTemp(): PostDto{
        let post: PostDto = new PostDto();
        post.avaliacaoNegativa = 10;
        post.id = 1;
        post.avaliacaoPositiva = 10;
        post.avaliacaoAtual = 0;
        post.fotoProduto = "https://avantare.com.br/wp-content/uploads/2020/04/teste-ab-1140x470.png";
        post.conteudo = "conteudo post";
        post.fotoUsuario = "https://avatarfiles.alphacoders.com/200/200998.png";
        post.idUsuario = 1;
        return post;
    }

    public converterPost(postDomain: post): PostDto{
        let post: PostDto = new PostDto();
        post.avaliacaoNegativa = 10;
        post.id = parseInt(postDomain.ID);
        post.avaliacaoPositiva = 10;
        post.avaliacaoAtual = 0;
        post.fotoProduto = postDomain.PRODUTO.IMAGEM;
        post.conteudo = postDomain.CONTEUDO;
        post.fotoUsuario = postDomain.USUARIO.FOTO;
        post.idUsuario = postDomain.USUARIO.ID;
        return post;
    }

    @Security('jwt')
    @Get("/listar/{id}")
    public async selecionar( id: string ): Promise<PostDto>{
        try {
            let posts: post[] = await injecao.postRepositorio.listar();
            let postFinded = posts.find( (x) => x.ID == id );
            if(postFinded != null ){
                this.setStatus(SatatusCode.OK());
                return this.converterPost(postFinded);
            } 
            else{
                this.setStatus( SatatusCode.NotFound() );
                return new PostDto();
            }
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return new PostDto();
        }
    }
    @Security('jwt')
    @Get("/listar/procurar/{pesquisa}")
    public async procurar( pesquisa: string): Promise<PostDto[]>{
        try {

            let posts: post[] = await injecao.postRepositorio.listar();
            let postFiltered: post[] = posts.filter( (x) => x.CONTEUDO.lastIndexOf(pesquisa) != -1 
                                                    || x.PRODUTO.NOME.lastIndexOf(pesquisa) !=-1 
                                                    || x.PRODUTO.LOJA.NOME.lastIndexOf(pesquisa) !=-1
                                                    || x.USUARIO.NOME.lastIndexOf(pesquisa) !=-1 );
            if(postFiltered != null ){
                this.setStatus(SatatusCode.OK());
                return postFiltered.map( x => this.converterPost(x));
            } 
            else{
                this.setStatus( SatatusCode.NotFound() );
                return [];
            }
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return [new PostDto()];
        }
    }
    @Security('jwt')
    @Get("/listar/")
    public async listar(): Promise<PostDto[]>{
        try {
            let posts: post[] = await injecao.postRepositorio.listar();
            if(posts != null ){
                this.setStatus(SatatusCode.OK());
                return posts.map( x => this.converterPost(x));
            } 
            else{
                this.setStatus( SatatusCode.NotFound() );
                return [];
            }
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return [new PostDto()];
        }
    }

    @Security('jwt')
    @Get("/listar/amizade")
    public async listarPorAmizade(): Promise<PostDto[]>{
        this.setStatus(SatatusCode.OK());
        return [ this.criarPostTemp() ];
    }

    @Security('jwt')
    @Get("/listar/loja")
    public async listarPorLoja(): Promise<PostDto[]>{
        this.setStatus(SatatusCode.OK());
        return [ this.criarPostTemp() ];
    }

    @Security('jwt')
    @Get("/listar/produto")
    public async listarPorProduto(): Promise<PostDto[]>{
        this.setStatus(SatatusCode.OK());
        return [ this.criarPostTemp() ];
    }

    @Security('jwt')
    @Post("/")
    public async criar( @Body() postDto: PostCriacaoDto, @Request() request: Express.Request ): Promise<string>{
        let postDomain: post = new post();
        const userId: string = ( (<any>request).userId );
        postDomain.CONTEUDO = postDto.conteudo;
        postDomain.PRODUTO = new produto();
        postDomain.PRODUTO.ID = parseInt(postDto.produto);
        postDomain.USUARIO = new usuario(injecao.manipuladorTexto);
        postDomain.USUARIO.ID = parseInt(userId);
        if( (await injecao.postRepositorio.criar(postDomain) ) ){
            this.setStatus(SatatusCode.OK());   
            return mensagens.operacaoSucesso;
        }else{
            this.setStatus(SatatusCode.InternalServerError());   
            return mensagens.operacaoFalha;
        }
    }

    @Security('jwt')
    @Put("/")
    public async alterar( @Body() post: PostCriacaoDto ): Promise<string>{
        this.setStatus(SatatusCode.OK());
        return mensagens.operacaoSucesso;
    }

    @Security('jwt')
    @Delete("/{postid}")
    public async deletar( postid: string ): Promise<string>{
        this.setStatus(SatatusCode.OK());
        return mensagens.operacaoSucesso;
    }

    @Security('jwt')
    @Post("/avaliar")
    public async avaliar( @Body() avaliacao: AvaliacaoDto ): Promise<string>{
        this.setStatus(SatatusCode.OK());
        return mensagens.operacaoSucesso;
    }

}
