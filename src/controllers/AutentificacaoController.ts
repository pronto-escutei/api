import injecao = require("../utils/Injector");
import jwt = require("jsonwebtoken");
import authConfig = require("../config/Autentificacao");

import LoginDto from '../dto/loginDto';
import AutentificacaoService from '../services/AutentificacaoService';
import usuario from '../dominio/usuario';
import UsuarioService from "../services/UsuarioService";

let _autentificacaoService: AutentificacaoService = injecao.autentificacaoService;
let _usuarioService: UsuarioService = injecao.UsuarioService;

import { Controller, Route, Get, Post, BodyProp, Body } from 'tsoa';
import SatatusCode from "../utils/impl/StatusCode";
import usuarioDto from "../dto/usuarioDto";

@Route("autentificacao")
export class AutentificacaoController extends Controller{

    @Post("/login")
    public async login( @Body() loginDto: LoginDto): Promise<any>{
        try{
            let validade: boolean = (await _autentificacaoService.login(loginDto.login, loginDto.senha))
            if(!LoginDto.validarCamposObrigatorios(loginDto)){
                this.setStatus(SatatusCode.BadRequest());
                return "Campos obrigatórios";
            }else if( validade ){
                let id: number = (await _usuarioService.buscarPorLogin(loginDto.login)).ID;
                const token: string = jwt.sign({ id: id.toString()}, authConfig.secrete.hash, { expiresIn: authConfig.secrete.tempoExpiracao } );
                this.setStatus(SatatusCode.OK());
                return token;
            }
            else{
                this.setStatus(SatatusCode.Unauthorized());
                return "sem autorização";
            }
        }catch(error){
            this.setStatus(SatatusCode.BadRequest());
            console.error(error.message);
            return error.message;
        }
    }
    
    @Post("/cadastro")
    public async cadastro( @Body() usuarioDtoResponse: usuarioDto): Promise<any>{
        const usuarioCadastro: usuario = await _autentificacaoService.converterUsuarioDtoParausuario(usuarioDtoResponse);

        if(await _autentificacaoService.validarUsuarioCadastro(usuarioCadastro)){
            if(await _autentificacaoService.cadastrar(usuarioCadastro)) this.setStatus(SatatusCode.OK());
            else this.setStatus(SatatusCode.InternalServerError());
            return "";
        }else{
            this.setStatus(SatatusCode.PreconditionFailed());
            return "Usuario já existe";
        }
    }
}