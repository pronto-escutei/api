import { Controller, Route, Get, Post, Security, Body } from 'tsoa';
import SatatusCode from "../utils/impl/StatusCode";
import LojaDto from '../dto/LojaDto';
import mensagens = require("./mensagens");
import injecao = require("../utils/Injector");
import loja from '../dominio/loja';

@Route("/loja")
export class LojaController extends Controller{
    
    public transformardomainEmDto(loj: loja): LojaDto{
        let lojaDto: LojaDto = new LojaDto();
        lojaDto.imagem = loj.IMAGEM;
        lojaDto.link = loj.LINK;
        lojaDto.nome = loj.NOME;
        lojaDto.id = parseInt(loj.ID);
        return lojaDto;
    }

    @Security('jwt')
    @Get("/{nome}")
    public async listar( nome: string ): Promise<LojaDto[]>{
        try {
            this.setStatus(SatatusCode.OK());
            let lojas: loja[] = await injecao.lojaReporitorio.listar();
            if(nome != "*"){
                lojas = lojas.filter( x => x.NOME.toLowerCase().indexOf(nome) != -1 );
            }
            return lojas.map( x => this.transformardomainEmDto(x) );
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return [];
        }
    }

    @Security('jwt')
    @Post("/")
    public async criar( @Body() lojaDto: LojaDto ): Promise<string>{
        try {
            this.setStatus(SatatusCode.OK());
            let lojaDomain: loja = new loja();
            lojaDomain.LINK = lojaDto.link;
            lojaDomain.IMAGEM = lojaDto.imagem;
            lojaDomain.NOME = lojaDto.nome;
            if(await injecao.lojaReporitorio.criar(lojaDomain)){
                this.setStatus(SatatusCode.OK());
                return mensagens.operacaoSucesso;
            }else{
                this.setStatus(SatatusCode.InternalServerError());
                return mensagens.operacaoFalha;
            }
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return error.message;
        }
    }

}
