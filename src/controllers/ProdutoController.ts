import { Controller, Route, Get, Post, Security, Body } from 'tsoa';
import SatatusCode from "../utils/impl/StatusCode";
import ProdutoDto from '../dto/ProdutoDto';
import injecao = require("../utils/Injector");

import mensagens = require("./mensagens");
import produto from '../dominio/produto';
import loja from '../dominio/loja';

@Route("/produto")
export class produtoController extends Controller{
    
    public transformarDomaiParaDto(prod: produto): ProdutoDto{
        let produtoDto: ProdutoDto = new ProdutoDto();
        produtoDto.IMAGEM = prod.IMAGEM;
        produtoDto.LINK = prod.LINK;
        produtoDto.produto_ID = parseInt(prod.LOJA.ID);
        produtoDto.ID = prod.ID;
        produtoDto.NOME = prod.NOME;
        produtoDto.NOMELOJA = prod.LOJA.NOME;
        return produtoDto;
    }

    @Security('jwt')
    @Get("/{nomeProduto}")
    public async listar(nomeProduto: string): Promise<ProdutoDto[]>{
        try {
            this.setStatus(SatatusCode.OK());
            let produtosDomain: produto[] = await injecao.produtoReporitorio.listar();
            if(nomeProduto != "*") produtosDomain = produtosDomain.filter( x => x.NOME.toLowerCase().indexOf(nomeProduto.toLowerCase()) != -1);
            return produtosDomain.map( x => this.transformarDomaiParaDto(x) );
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            console.error(error.message);
        }
    }

    @Security('jwt')
    @Post("/")
    public async criar( @Body() produtoDto: ProdutoDto ): Promise<string>{
        try {
            this.setStatus(SatatusCode.OK());
            let produtoDomain: produto = new produto();
            produtoDomain.LINK = produtoDto.LINK;
            produtoDomain.IMAGEM = produtoDto.IMAGEM;
            produtoDomain.NOME = produtoDto.NOME;
            produtoDomain.LOJA = new loja();
            produtoDomain.LOJA.ID = produtoDto.LOJA_ID.toString();
            if(await injecao.produtoReporitorio.criar(produtoDomain)){
                this.setStatus(SatatusCode.OK());
                return mensagens.operacaoSucesso;
            }else{
                this.setStatus(SatatusCode.InternalServerError());
                return mensagens.operacaoFalha;
            }
        } catch (error) {
            console.error(error.message);
            this.setStatus( SatatusCode.InternalServerError() );
            return error.message;
        }
    }

}
