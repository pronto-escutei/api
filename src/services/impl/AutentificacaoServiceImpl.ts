import usuario from "../../dominio/usuario";
import AutentificacaoService from "../AutentificacaoService";
import Security from "../../utils/Security";
import UsuarioRepositorio from "../../infra/UsuarioRepositorio";
import ManipuladorTexto from "../../utils/ManipuladorTexto";
import usuarioDto from "../../dto/usuarioDto";

export default class AutentificacaoServiceImpl implements AutentificacaoService{
    
    private _security: Security;
    private _usuariorepositorio: UsuarioRepositorio;
    private _manipuladortexto: ManipuladorTexto;

    constructor(security: Security, usuariorepositorio: UsuarioRepositorio, manipuladortexto: ManipuladorTexto){
        this._security = security;
        this._usuariorepositorio = usuariorepositorio;
        this._manipuladortexto = manipuladortexto;
    }

    async login(login: string, senha: string): Promise<boolean>{
        let usuario: usuario = await this._usuariorepositorio.buscarPorEmail(login);
        if(!usuario) return false;
        let validate: boolean = (await this._security.compararValores(senha, usuario.SALT, usuario.SENHA));
        return validate;
    }

    async cadastrar(usuario: usuario): Promise<boolean>{
        return this._usuariorepositorio.criar(usuario);
    }

    async validarUsuarioCadastro(usuario: usuario): Promise<boolean>{
        let usuarioEncontrado: usuario = await this._usuariorepositorio.buscarPorEmail(usuario.EMAIL);
        return usuarioEncontrado == undefined && usuarioEncontrado == null && usuario.validarCamposObrigatorios();
    }

    async converterUsuarioDtoParausuario(usuarioDto: usuarioDto): Promise<usuario>{
        let salt = await this._security.gerarSalt(usuarioDto.SENHA);
        let hash = await this._security.gerarHash(usuarioDto.SENHA, salt);
        let user: usuario = new usuario(this._manipuladortexto);
        user.EMAIL = usuarioDto.EMAIL;
        user.ID_FACEBOOK = usuarioDto.ID_FACEBOOK;
        user.NOME = usuarioDto.NOME;
        user.SALT = salt;
        user.SENHA = hash;
        return user;
    }
}