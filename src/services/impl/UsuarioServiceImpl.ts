import usuario from "../../dominio/usuario";
import UsuarioService from "../UsuarioService";
import UsuarioRepositorio from "../../infra/UsuarioRepositorio";
import Security from "../../utils/Security";

export default class UsuarioServiceImpl implements UsuarioService{

    _usuarioRepositorio: UsuarioRepositorio;   
    private _security: Security;

    constructor(usuarioRepositorio: UsuarioRepositorio, security: Security,){
        this._usuarioRepositorio = usuarioRepositorio;
        this._security = security;
    }

    async buscarPorLogin(login: string): Promise<usuario>{
        return await this._usuarioRepositorio.buscarPorEmail(login);
    }
    async buscarPorId(id: number): Promise<usuario>{
        return await this._usuarioRepositorio.buscarPorId(id);
        
    }
    async buscarAmizades(usuario: usuario): Promise<usuario[]>{
        return [];
    }
    async alterar(usuario: usuario): Promise<boolean>{
        if(usuario.SENHA){
            let salt = await this._security.gerarSalt(usuario.SENHA);
            let hash = await this._security.gerarHash(usuario.SENHA, salt);
            usuario.SALT = salt;
            usuario.SENHA = hash;
        }
        return await this._usuarioRepositorio.atualizar(usuario);
    }

}
