import usuario from "../dominio/usuario";

export default interface UsuarioService{
    buscarPorLogin(login: string): Promise<usuario>;
    buscarPorId(id: number): Promise<usuario>;
    buscarAmizades(usuario: usuario): Promise<usuario[]>;
    alterar(usuario: usuario): Promise<boolean>;
}
