import usuario from "../dominio/usuario";
import usuarioDto from "../dto/usuarioDto";

export default interface AutentificacaoService{

    login(login: string, senha: string): Promise<boolean>
    cadastrar(usuario: usuario): Promise<boolean>
    validarUsuarioCadastro(usuario: usuario): Promise<boolean>
    converterUsuarioDtoParausuario(usuarioDto: usuarioDto): Promise<usuario>
}