const express = require('express');
const app = express();
const bodyParser = require('body-parser');
import { Request, Response } from 'express';
const cors = require('cors');

import * as route from './routes'

app.use(cors());
app.set("port", process.env.PORT || 3000);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const swaggerUi = require("swagger-ui-express");
import * as swaggerDocs from './swagger.json';
//import authMiddleware from './middleware/auth';

// app.use("teste", authMiddleware);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

route.RegisterRoutes(app);


/**
 * @swagger
 * /teste
 * get:
 *  description: só teste mesmo
 *  responses:
 *    '200': OK
 *      description: tudo ok
 */
app.get("/", (req: Request, res: Response) => {  res.status(200).send("teste inicial") })

export default app;


