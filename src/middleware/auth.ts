import { Request, Response } from 'express';
import jwt = require("jsonwebtoken");
import StatusCode from '../utils/impl/StatusCode';
import authConfig = require("../config/Autentificacao");
import { UserAuthInfoRequest } from '../utils/UserAuthInfoRequest';

function baseError(res: Response, msg: string){
    return res.status(StatusCode.BadRequest()).send( { error: msg } )
}

let authMiddleware = function(req: UserAuthInfoRequest, res: Response, next: any) {
    const authHeader = req.headers.authorization;
    
    if(!authHeader) return baseError(res, 'sem provedor de token');

    const parts = authHeader.split(' ');
    if(parts.length != 2) return baseError(res, 'erro token');

    if(!/^Bearer$/i.test(parts[0])) return baseError(res, 'token mal formatado');

    jwt.verify(parts[1], authConfig.secrete.hash, (err, decoded: { id: number }) => {
        if(err) return baseError(res, 'token inválido');
        req.userId = decoded.id;
        next();
    });
}

export default authMiddleware;