// Auth.ts
import { Request, Response } from 'express';
import jwt = require("jsonwebtoken");
import StatusCode from '../utils/impl/StatusCode';
import authConfig = require("../config/Autentificacao");
import { UserAuthInfoRequest } from '../utils/UserAuthInfoRequest';

function baseError(msg: string): any{
    return { error: msg, status: StatusCode.Unauthorized() };
}

export function expressAuthentication(
    req: UserAuthInfoRequest,
    securityName: string,
    scopes?: string[]
): Promise<any> {
    const authHeader = req.headers.authorization;
    return new Promise((resolve, reject) => {

        if(!authHeader) return reject(baseError('sem provedor de token'));

        const parts = authHeader.split(' ');
        if(parts.length != 2) return reject(baseError('erro token'));
        
        if(!/^Bearer$/i.test(parts[0])) return reject(baseError('token mal formatado'));

        jwt.verify(parts[1], authConfig.secrete.hash, (err, decoded: { id: number }) => {
            if(err) return reject(baseError('token inválido'));
            req.userId = decoded.id;
            resolve(decoded);
        });
    });
}