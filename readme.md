# Pronto, falei

## RESTful API com Node and TypeScript

* inicializar projeto: `npm install`

* rodar projeto com alteração do arquivo: `npm run watch`

* rodar somente a api: `node dist/server.js`

* compilação typescript: `tsc`

* rodar testes: `jasmine`
